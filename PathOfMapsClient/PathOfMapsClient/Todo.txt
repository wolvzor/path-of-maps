﻿- Scroll to bottom on item added
- (server-side) fix "The Body" and "Maelström of Chaos Mountain Ledge Map" not logging
- Color item drops (see slack for colors), items show up as icons with hover text + quantity (as on the PoM site)
- Input overlay on fullscreen-windowed PoE window to enter commands
- Dynamic Zana mods loading
- Alert on dangerous mods
- Edit map button
- Add note
- Add fragment
- Open special map (atziri, uber, pale court, zana map, corrupted zone)
- Found trial (for uber lab)
- Fix other users commanding a user of this PoM desktop application by tricking the regex (make it more strict).

Settings:
- Show dialog on end/abort/reopen map -> ON/OFF

No priority:
- Timer for when running a map
- Re-open map (fetch data from current map?)
- Pause + resume map
- Manually start map (input IIQ/IIR etc.)
- Show changelog on update found
- Multilanguage? (hard to realise on the server side)
- Add webserver <--> mobile connection, ability to control/send commands from the mobile application