﻿using Newtonsoft.Json;
using PathOfMapsClient.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PathOfMapsClient.HelperClasses {
    class Updater {
        private static string UPDATE_URL = "http://pathofmaps.com/releases.json";
        private static string CURRENT_VERSION = "1.1.0";

        public static async Task<UpdateObj> CheckForUpdates() {
            try {
                using (HttpClient client = new HttpClient()) {
                    string update_json = await client.GetStringAsync(new Uri(UPDATE_URL + "?current=" + CURRENT_VERSION));
                    UpdateObj update = JsonConvert.DeserializeObject<UpdateObj>(update_json);
                    return update;
                }
            }
            catch (Exception) {
                return new UpdateObj();
                // Todo: log?
            }
        }
    }
}
