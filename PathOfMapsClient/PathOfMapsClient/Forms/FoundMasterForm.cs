﻿using System;
using System.Windows.Forms;

namespace PathOfMapsClient {
    public partial class FoundMasterForm : Form {
        public FoundMasterForm() {
            InitializeComponent();
        }

        public enum Masters {
            NO_MASTER,
            HAKU,
            ELREON,
            CATARINA,
            TORA,
            VORICI,
            VAGAN,
            ZANA
        }

        public Masters SelectedMaster { get; set; }

        private void btnMasterClick(object sender, EventArgs e) {
            switch (((Control)sender).Name) {
                case "picHaku":
                    SelectedMaster = Masters.HAKU;
                    break;
                case "picElreon":
                    SelectedMaster = Masters.ELREON;
                    break;
                case "picCatarina":
                    SelectedMaster = Masters.CATARINA;
                    break;
                case "picTora":
                    SelectedMaster = Masters.TORA;
                    break;
                case "picVorici":
                    SelectedMaster = Masters.VORICI;
                    break;
                case "picVagan":
                    SelectedMaster = Masters.VAGAN;
                    break;
                case "picZana":
                    SelectedMaster = Masters.ZANA;
                    break;
                case "btnNoMaster":
                default:
                    SelectedMaster = Masters.NO_MASTER;
                    break;
            }

            this.Close();
        }
    }
}
